const context = require.context('./', false, /\.(png|jpe?g|svg)$/);
const images = context.keys().map(context);

export default images;
